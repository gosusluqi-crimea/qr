import React from 'react'
import { HashRouter as Router, Route, Switch } from 'react-router-dom'

import QrCode from './QrCode'
import QrGenerator from './QrGenerator'

import './App.css'

function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Switch>
        <Route exact path="/generate" component={QrGenerator}>
        </Route>
        <Route exact path="/validate" component={QrCode}>
        </Route>
      </Switch>
    </Router>
  )
}

export default App
