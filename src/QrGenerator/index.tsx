// @ts-ignore
import { useState, useEffect } from 'react'
import queryString from 'query-string'
import QRCode from 'qrcode'
import { getHiddenName, getHiddenPassport } from '../helpers'
import { RouteComponentProps } from 'react-router-dom'

import './index.css'

const QrGenerator = (props: RouteComponentProps<{}>) => {
  const [name, setName] = useState<string>('')
  const [series, setSeries] = useState<string>('')
  const [birth, setBirth] = useState<string>('')
  const [image, setImage] = useState<any>(null)
  const query = queryString.parse(props.location.search)

  useEffect(() => {
    if (query.name) {
      setName(query.name as string)
    }

    if (query.series) {
      setSeries(query.series as string)
    }

    if (query.birth) {
      setBirth(query.birth as string)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const setValue = (key: string, value: string) => {
    const newQuery = {
      ...query
    }

    if (key === 'name') {
      newQuery[key] = getHiddenName(value)
    } else if (key === 'series') {
      newQuery[key] = getHiddenPassport(value)
    } else {
      newQuery[key] = value
    }

    props.history.replace(
      `${props.location.pathname}?${queryString.stringify(newQuery)}`
    )
  }

  useEffect(() => {
    ;(async () => {
      if (name && series && birth) {
        try {
          const url = await QRCode.toDataURL(
            `${window.location.origin + props.location.pathname}#/validate?${queryString.stringify({
              name: getHiddenName(name),
              series: getHiddenPassport(series),
              birth,
            })}`
          )

          setImage(url)
        } catch (err) {
          console.error(err)
        }
      }
    })()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [name, series, birth])

  return (
    <div className="qr-generator">
      <div>
        <label htmlFor="name">ФИО</label>
        <br />
        <input
          type="text"
          name="name"
          value={name}
          onChange={(e) => {
            setName(e.target.value)
            setValue('name', e.target.value)
          }}
        />
      </div>
      <div>
        <label htmlFor="passport">Номер Паспорта</label>
        <br />
        <input
          type="text"
          name="passport"
          value={series}
          onChange={(e) => {
            setSeries(e.target.value)
            setValue('series', e.target.value)
          }}
        />
      </div>

      <div>
        <label htmlFor="birthdate">Дата рождения ДД.ММ.ГГГГ</label>
        <br />
        <input
          name="birthdate"
          type="text"
          value={birth}
          onChange={(e) => {
            setBirth(e.target.value)
            setValue('birth', e.target.value)
          }}
        />
      </div>
      <div className="qr-generator__image">
        {image ? (
          <>
            <img src={image} alt="qr_code" />
            <a className="btn" href={image} download="qrcode.png">
              Скачать
            </a>
          </>
        ) : null}
      </div>
    </div>
  )
}

export default QrGenerator
