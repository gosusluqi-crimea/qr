import add from 'date-fns/add'
import format from 'date-fns/format'
import queryString from 'query-string'
import { getHiddenName, getHiddenPassport } from '../helpers'
import { RouteComponentProps } from 'react-router-dom'

import { ReactComponent as EnIcon } from './en.svg'
import { ReactComponent as RuIcon } from './ru.svg'
import { ReactComponent as CompleteIcon } from './complete.svg'
import { ReactComponent as NormalIcon } from './normal.svg'
import { ReactComponent as InvalidIcon } from './normal.svg'
import { ReactComponent as PersonDate } from './person-date.svg'

const getRandomNumber = (format: string = '') => {
  return format.replace(/n/g, (str) => {
    return Math.round(Math.random() * 9).toString()
  })
}

const QrCode = (props: RouteComponentProps<{}>) => {
  const query = queryString.parse(props.location.search)
  const date = add(new Date(), {
    months: 4,
    days: 19,
  })

  return (
    <div className="vaccine-result">
      <div className="flex-container ml-6 mr-6 justify-between align-items-center mt-52 mb-32">
        <div className="ml-24">
          <a href="https://www.gosuslugi.ru/" className="logo">a</a>
        </div>
        <div className="translate-button flex-container mt-6 mr-24 align-items-center">
          <div className="mr-8">
            <div className="lang-image ru">
              <div className="lang-en">
                <EnIcon />
              </div>
              <div className="lang-ru">
                <RuIcon />
              </div>
            </div>
          </div>
          <div className="lang">RUS</div>
        </div>
      </div>
      <div>
        <div className="status-container mt-28">
          <div className="complete-image status-container-image">
            <CompleteIcon />
          </div>
          <div className="normal-image status-container-image">
            <NormalIcon />
          </div>
          <div className="invalid-image status-container-image">
            <InvalidIcon />
          </div>
          <div className="status-container-inner">
            <h4 className="title-h4 white status-title main-title">
              Сертификат вакцинации от COVID-19
            </h4>
            <div className="status mt-12 text-plain small-text bold">
              <span className="status-value hide not-found"></span>
              <span className="status-value cert-name">Действителен</span>
            </div>
            <h4 className="title-h4 white status-title mt-12">
              <span className="num-symbol">№</span>{' '}
              <span className="unrz">
                {getRandomNumber('nnnn nnnn nnnn nnnn')}
              </span>
            </h4>
          </div>
        </div>

        <div className="qr-container hide"></div>

        <div className="qr-number hide"></div>

        <div className="person-data person-data-dates mt-12">
          <div className="mb-4 person-data-wrap align-items-center">
            <div className="person-date mr-12">
              <PersonDate />
            </div>
            <div className="small-text gray mr-4">Действует до: </div>
            <div className="small-text gray">{format(date, 'dd.MM.yyyy')}</div>
          </div>
        </div>

        <div className="person-data person-data-attrs mt-24">
          <div className="mb-4 person-data-wrap attr-wrap">
            <div className="small-text mb-4 mr-4 attr-title hide">ФИО: </div>
            <div className="attrValue title-h6 bold text-center">
              {getHiddenName(query.name ?? '')}
            </div>
          </div>
          <div className="mb-4 person-data-wrap attr-wrap">
            <div className="small-text mb-4 mr-4 attr-title">
              Дата рождения:{' '}
            </div>
            <div className="attrValue small-text gray">{query.birth}</div>
          </div>
          <div className="mb-4 person-data-wrap attr-wrap">
            <div className="small-text mb-4 mr-4 attr-title">Паспорт: </div>
            <div className="attrValue small-text gray">{getHiddenPassport(query.series ?? '')}</div>
          </div>
        </div>

        <div className="mt-24">
          <a href="https://www.gosuslugi.ru/" className="button close">
            Закрыть
          </a>
        </div>
      </div>
    </div>
  )
}

export default QrCode
