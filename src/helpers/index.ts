export const getHiddenName = (name: string | string[] = '') => {
  let hiddenName = ''

  for (let i = 0; i < name.length; i++) {
    if (i === 0 || name[i - 1] === ' ' || name[i] === ' ') {
      hiddenName = hiddenName + name[i].toUpperCase()
    } else {
      hiddenName = hiddenName + '*'
    }
  }

  return hiddenName
}

export const getHiddenPassport = (series: string | string[] = '') => {
  return `${series.slice(0, 2)}** ***${series.slice(-3)}`
}
